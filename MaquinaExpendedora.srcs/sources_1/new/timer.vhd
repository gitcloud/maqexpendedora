--TIMER: temporizador para estado de espera de la m�quina de estados
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity timer is
  generic(
      clockFrequency : integer := 100e6; -- Frec reloj 100MHz
      delay			 : integer := 3      -- Espera 3 segundos      
  );
  port(
      clk   : in std_logic;   -- Clock input
      reset : in std_logic;   -- Reset
      load	: in std_logic;   -- Entrada indicadora de carga
      zero	: out std_logic	  -- Salida indicadora de fin de la espera
  );
end entity;
 
architecture behavioral of timer is
 
    -- Se�al para contar hacia atr�s en cada flanco positivo de reloj
    signal ticks : integer := 0; -- se cargar� cuando load se active
 
begin
 
    process(clk, reset, load) is
    begin
    	-- si RESET a 1
    	if reset = '1' then
        	ticks   <=  0;
            zero	<= '0';
            
    	-- Flanco positivo de reloj
        elsif rising_edge(clk) then
 
            if load = '1' then
            	ticks   <= delay * clockFrequency;
                zero	<= '0';
                
            -- Se�al de carga desactivada
            else
 				-- Fin de la cuenta
                if ticks = 0 then
                	zero <= '1';
                -- Decrementar ticks
                else
                	ticks <= ticks - 1;
                    zero  <= '0';
 				end if;
                
            end if;
            
        end if;
        
    end process;
 
end architecture;
