LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

ENTITY top IS
    PORT ( 
        RESET   : IN std_logic;
        CLK     : IN std_logic;
        COINS   : IN std_logic_vector(3 downto 0);
        PRODUCT : OUT std_logic;
        ERROR_OUT   : OUT std_logic;
        LIGHT   : OUT std_logic_vector(1 downto 0)
    );
END top;

ARCHITECTURE dataflow OF top IS
    signal SYNC            : std_logic_vector(3 downto 0);
    signal EDGE            : std_logic;
    signal monedas         : std_logic_vector(3 downto 0);
    signal ERROR           : std_logic;
    signal SUFIC           : std_logic;

COMPONENT SYNCHRNZR
    PORT (
       CLK      : in std_logic;
       ASYNC_IN : in std_logic;
       SYNC_OUT : out std_logic
    );
END COMPONENT;

COMPONENT EDGEDTCTR
    port (
        CLK     : in std_logic;
        SYNC_IN : in std_logic;
        EDGE    : out std_logic
    );
END COMPONENT;

COMPONENT count
    port (
        CLK       : in  std_logic;
        RESET     : in  std_logic;
        COINS	  : in std_logic_vector(3 downto 0); --[100,50,20,10]
		ENOUGH	  : out std_logic;
        ERROR	  : out std_logic
    );
END COMPONENT;

COMPONENT fsm
    port(
         RESET     : in  std_logic;
         CLK       : in  std_logic;
         ENOUGH    : in  std_logic;
         ERROR     : in  std_logic;
         PRODUCT   : out std_logic;
         ERROR_OUT : out std_logic;
         LIGHT	   : out std_logic_vector(1 downto 0)
    );
 END COMPONENT;
   
BEGIN
--Sincronizadores (1 por cada entrada de monedas)
Inst_SYNCHRNZR3:SYNCHRNZR PORT MAP(
    CLK      => CLK,
    ASYNC_IN => COINS(3),
    SYNC_OUT => SYNC(3)
);

Inst_SYNCHRNZR2:SYNCHRNZR PORT MAP(
    CLK      => CLK,
    ASYNC_IN => COINS(2),
    SYNC_OUT => SYNC(2)
);

Inst_SYNCHRNZR1:SYNCHRNZR PORT MAP(
    CLK      => CLK,
    ASYNC_IN => COINS(1),
    SYNC_OUT => SYNC(1)
);

Inst_SYNCHRNZR0:SYNCHRNZR PORT MAP(
    CLK      => CLK,
    ASYNC_IN => COINS(0),
    SYNC_OUT => SYNC(0)
);

--Detectores de flancos (1 por cada salida de sincronizador)
Inst_EDGEDTCTR3:EDGEDTCTR PORT MAP(
    CLK     => CLK,
    SYNC_IN => SYNC(3),
    EDGE    => monedas(3)
);

Inst_EDGEDTCTR2:EDGEDTCTR PORT MAP(
    CLK     => CLK,
    SYNC_IN => SYNC(2),
    EDGE    => monedas(2)
);

Inst_EDGEDTCTR1:EDGEDTCTR PORT MAP(
    CLK     => CLK,
    SYNC_IN => SYNC(1),
    EDGE    => monedas(1)
);

Inst_EDGEDTCTR0:EDGEDTCTR PORT MAP(
    CLK     => CLK,
    SYNC_IN => SYNC(0),
    EDGE    => monedas(0)
);

--Contador de monedas
Inst_count:count PORT MAP(
    CLK    => CLK,
    RESET  => RESET,
    COINS  => monedas,
    ENOUGH => SUFIC,
    ERROR  => ERROR
);

Inst_fsm:fsm PORT MAP(
    RESET     => RESET,
    CLK       => CLK,
    ENOUGH    => SUFIC,
    ERROR     => ERROR,
    ERROR_OUT =>ERROR_OUT,
    PRODUCT   => PRODUCT,
    LIGHT	  => LIGHT
);

END ARCHITECTURE dataflow;