--DEBOUNCER: evita rebotes en entradas pushbutton

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity debouncer is
  Port (
    CLK : in std_logic;
    SYNC_IN : in std_logic;
    SYNCED_OUT : out std_logic
  );
end debouncer;

architecture Behavioral of debouncer is
    signal Q1, Q2, Q3 : std_logic;
begin
    process(clk)
    begin
       if (clk'event and clk = '1') then
            Q1 <= SYNC_IN;
            Q2 <= Q1;
            Q3 <= Q2;
       end if;
    end process;
    
    SYNCED_OUT <= Q1 and Q2 and (not Q3);

end Behavioral;
