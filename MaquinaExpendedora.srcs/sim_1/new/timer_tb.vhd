-- Timer testbench
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer_tb is
end timer_tb;

architecture behavioral of timer_tb is 
	
  --Inputs
  signal clk   : std_logic;
  signal reset : std_logic;
  signal load  : std_logic;

  --Outputs
  signal zero  : std_logic;
  
  -- Component Declaration for the Unit Under Test (UUT)
  component timer is
  	generic(
      clockFrequency : integer := 10; -- Frec reloj 10MHz
      delay			 : integer := 1	  -- Espera 1 segundo      
  	);
  	port(
      clk   : in std_logic;   -- Clock input
      reset : in std_logic;   -- Reset
      load	: in std_logic;   -- Entrada indicadora de carga
      zero	: out std_logic	  -- Salida indicadora de fin de la espera
  	);
  end component;

  -- Clock period definitions
  constant clk_period : time := 100 ms;

begin
  -- Instantiate the Unit Under Test (UUT)
  uut: timer
    port map (
      clk      => clk,
      reset    => reset,
      load     => load,
      zero     => zero
    );

  -- Clock
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;
	
  -- Reset signal
  reset <= '0', '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;

  -- Stimulus
  load <= '0', '1' after 1 * clk_period, '0' after 2 * clk_period;
  
  
  end_sim: process
  begin
    wait for 15 * clk_period;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;
  
end;
